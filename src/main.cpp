/*
*  ------------- KOMATSU REMAN CENTER CHILE --------------------
*
*   Developer: Ingeniero de innovación y desarrollo, Felipe Bravo S.
*   
*   Code extracted from various sources on the internet
*   
*   REF 1: https://techtutorialsx.com/2017/04/09/esp8266-connecting-to-mqtt-broker/
*   REF 2: http://www.esp8266learning.com/wemos-webserver-example.php
*   
*   Among others.
*   
*   Control topic: esp/LED/STATE
*   Control Message:   "ON" or "OFF"
*   
*   Status topic: esp/LED/STATUS
*   Status message:    "ON" or "OFF"
*      
*   
*/


/*==========================================================
*              LIBRARIES
==========================================================*/


#include <ESP8266WiFi.h>
#include <PubSubClient.h> 


/*==========================================================
*              GLOBAL VARIABLES
==========================================================*/

// THE NODE ID NUMBER
const String numberId = "02";

const char* ssid = "Ammyfloam";  
const char* password = "Ammy051403";
const char* mqttServer = "192.168.100.59";    //url MQTT broker
const int mqttPort = 1883;   // MQTT port, default is 1883

const String clientId = "esp" + numberId; //unique ID, change this to something else arbitrary, anything you want with letters and numbers
const String name = "Nodo " + numberId;

const String ledTopic= clientId+"/led/state";
const String ledStatusTopic= clientId+"/led/status";
const String nodeData= clientId+"/node/data";
const String msgTopic= clientId+"/system/message";
String nodeDataJson = "{\"id\":\""+clientId+"\",\"name\":\""+name+"\",\"type\":\"light\",\"states\":[\"OFF\",\"ON\"]}";
char charArray[100];

const int heartBeatTime = 5000; //hearbeat message time in milliseconds
int lastHeartBeat = 0; // register the last hearbeat in millis

int value = HIGH;

int WEB_TIME_LIMIT = 2000;  //time limit set to 2000 miliseconds
  
WiFiServer server(80);
WiFiClient espClient;
PubSubClient client(espClient);



/*==========================================================
*              HARDWARE SETUP FUNCTIONS
==========================================================*/

void prepareTopic(String stringOne) {  
    stringOne.toCharArray(charArray,100);
}

void hardwareStart(){
  Serial.begin(9600);
  delay(10); 
  // we use the built-in name for the built-in LED, no need to look on the schematics
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);  
}

int do_request(String request){
  if (request.indexOf("/LED=OFF") != -1) {
    
    // change the pin state to HIIGH, which will turn the led OFF due to the pull-up resistor
    digitalWrite(LED_BUILTIN, HIGH);
    // publish the change in the MQTT broker
    prepareTopic(ledStatusTopic);
    client.publish(charArray, "OFF");
    value = HIGH;
    
  }  else if (request.indexOf("/LED=ON") != -1){ 
       
    // change the pin state to LOW, which will turn the led ON due to the pull-up resistor
    digitalWrite(LED_BUILTIN, LOW);
    // publish the change in the MQTT broker
    prepareTopic(ledStatusTopic);
    client.publish(charArray, "ON");
    value = LOW;
  }  
  return value;
}

/*==========================================================
*              MQTT FUNCTIONS
==========================================================*/


void heartBeat() {
  if ( millis() - lastHeartBeat > heartBeatTime )
  {
    lastHeartBeat = millis();
    prepareTopic(nodeData);
    value = !digitalRead(LED_BUILTIN);
    String status = (value) ? "ON" : "OFF";
    nodeDataJson =  "{\"id\":\"" + clientId
                    +"\",\"name\":\"" + name
                    +"\",\"type\":\"light\""
                    +",\"states\":[\"OFF\",\"ON\"],"
                    +"\"status\":\"" + status
                    +"\"}";
    int str_len = nodeDataJson.length() + 1; 
    char char_array[str_len];
    nodeDataJson.toCharArray(char_array, str_len);
    client.publish(charArray, char_array); 
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
 
  Serial.println();
  Serial.println("-----------------------");

  char *message = (char *) payload;  
  // Making sure there's an end to the string, otherwise it will leak into memory
  message[length]=(char)0;
  prepareTopic(ledTopic);
  if(strcmp(topic,charArray)==0){
      Serial.println("------- LED TOPIC --------");
      if(strcmp(message,"ON")==0){
        Serial.println("STATE: ON");
        String request = "/LED=ON";
        do_request(request);
      } else if(strcmp(message,"OFF")==0){
        Serial.println("STATE: OFF");        
        String request = "/LED=OFF";
        do_request(request);          
      } else {
        Serial.println(message);
      }
  }
 
}

void mqtt_connect(){
  
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
  
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
    
    prepareTopic(clientId);
    if (client.connect(charArray)) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }   
  
  heartBeat();
  prepareTopic(ledTopic);
  client.subscribe(charArray);
  client.publish(charArray,charArray); 
}


/*==========================================================
*              WIFI FUNCTIONS
==========================================================*/

void wifi_connect(){
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
 
  // Start the server
  server.begin();
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL : ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");
}



/*==========================================================
*              SETUP
==========================================================*/
 
void setup() {
  // start serial COM and LED output
  hardwareStart();
  // Connect to WiFi network
  wifi_connect();
  // connect to MQTT broker
  mqtt_connect(); 
}


/*==========================================================
*              LOOP
==========================================================*/


void loop() {
  client.loop(); 
  // heartBeat or "stay alive"
  heartBeat();
}

